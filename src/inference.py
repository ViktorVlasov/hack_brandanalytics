import pandas as pd
import torch
from transformers import AutoTokenizer, T5ForConditionalGeneration
from tqdm import tqdm
import argparse
from bs4 import BeautifulSoup
import re

# Функция для очистки HTML
def clean_html(raw_html):
    soup = BeautifulSoup(raw_html, "html.parser")
    return soup.get_text()

def remove_urls(text):
    url_pattern = re.compile(r'https?://\S+|www\.\S+')
    return url_pattern.sub(r'', text)

# Функция для сортировки комментариев в посте по дате
def sort_comments_by_date(post_comments):
    return sorted(post_comments, key=lambda x: x['date_comments'])


# Функция для склеивания комментариев в один текст
def concat_commentaries(comments_dict):
    if len(comments_dict) == 0:
        return ""
    return '\n\n'.join([comment['text_comment'] for comment in comments_dict])


def preprocess_data(input_records_path, type_sum='all_comments'):
    MIN_LENGTH = 1 # минимальная длина комментария (в символах)
    MAX_LENGTH = 5000 # максимальная длина комментария (в символах)

    # Считываем все данные
    df = pd.read_json(input_records_path, lines=True, orient='records')
    
    # Отделяем посты
    posts = df[(df['root_id'].isna()) & (df['parent_id'].isna())].copy()
    posts.rename(columns={'text': 'text_post'}, inplace=True)
    posts = posts.drop(['root_id', 'parent_id'], axis=1)

    if type_sum != 'all_comments':
        posts['text_post'] = posts['text_post'].apply(clean_html).apply(remove_urls)
    
    # Отделяем комментарии
    post_comments = df[df['root_id'].notna()].copy()
    post_comments = post_comments.reset_index(drop=True)
    post_comments.rename(columns={'text': 'text_comment'}, inplace=True)

    # Чистим HTML и убираем ссылки в комментариях
    post_comments['text_comment'] = post_comments['text_comment'].apply(clean_html).apply(remove_urls)
    post_comments = post_comments[(post_comments['text_comment'].apply(len) >= MIN_LENGTH) & \
                                (post_comments['text_comment'].apply(len) <= MAX_LENGTH)]        

    # Объединяем данные
    merged_df = pd.merge(posts, post_comments, left_on='id', right_on='root_id', suffixes=('', '_comments'))

    # Группируем данные и создаем список словарей комментариев для каждого поста
    grouped_df = merged_df.groupby('id').apply(lambda group: group[['text_comment', 
                                                                    'url_comments', 
                                                                    'id_comments', 
                                                                    'hash_comments', 
                                                                    'date_comments', 
                                                                    'root_id', 
                                                                    'parent_id']].to_dict(orient='records')).reset_index(name='comments')

    # Объединяем с исходными постами
    posts = pd.merge(posts, grouped_df, on='id')

    posts['comments'] = posts['comments'].apply(sort_comments_by_date) # сортировка комментариев по дате
    posts['comments_concat'] = posts['comments'].apply(concat_commentaries) # конкатенация комментариев

    return posts

def gen_batch(inputs, batch_size):
    batch_start = 0
    while batch_start < len(inputs):
        yield inputs[batch_start: batch_start + batch_size]
        batch_start += batch_size

def predict(
    model_name,
    input_records_path,
    output_file='./result.jsonl',
    type_sum = 'all_comments',
    max_source_tokens_count=800,
    batch_size=8
):
    df = preprocess_data(input_records_path, type_sum)

    device = "cuda" if torch.cuda.is_available() else "cpu"

    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = T5ForConditionalGeneration.from_pretrained(model_name).to(device)

    summaries = []
    post_hashes = []
    hash_comments = df.comments.apply(lambda x: [i['hash_comments'] for i in x])
    
    input_records = df.to_dict('records')
    for batch in tqdm(gen_batch(input_records, batch_size)):
        texts = [r["comments_concat"] for r in batch]
        hashes = [r["hash"] for r in batch]

        input_ids = tokenizer(
            texts,
            add_special_tokens=True,
            max_length=max_source_tokens_count,
            padding="max_length",
            truncation=True,
            return_tensors="pt"
        )["input_ids"].to(device)

        output_ids = model.generate(
            input_ids=input_ids,
            no_repeat_ngram_size=4
        )
        summaries_batch = tokenizer.batch_decode(output_ids, skip_special_tokens=True)
        summaries.extend(summaries_batch)
        post_hashes.extend(hashes)

    pd.DataFrame({"summary": summaries, 
                  "post_hash": post_hashes, 
                  "comments_hash": hash_comments}).to_json(output_file, 
                                                            orient='records', 
                                                            lines=True)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script for making predictions.")
    
    parser.add_argument(
        "--type_summarization",
        type=str,
        default="all_comments",
        help="Type of summarization"
    )
    parser.add_argument(
        "--dataset_path",
        type=str,
        required=True,
        help="Path to the dataset"
    )
    parser.add_argument(
        "--result_path",
        type=str,
        default="./result.jsonl",
        help="Path to the output file"
    )

    args = parser.parse_args()

    # Вызов функции predict с передачей аргументов из командной строки
    predict(
        model_name="rhythm00/finetune_t5_base_only_hack",
        input_records_path=args.dataset_path,
        output_file=args.result_path,
        type_sum=args.type_summarization
    )