# hack_brandanalytics

Репозиторий проекта по суммаризации комментариев в социальных сетях.

## Минимальные требования
    Python >= 3.10
## Установка зависимостей.
Все зависимости описаны в файле requirements.txt и в pyproject.toml
Для загрузки всех зависимостей можно использовать один из двух способов.

Первый способ:
1. Установите poetry
```bash
pipx install poetry
```
2. Выполните в директории проекта:
```bash
poetry install
```

Второй способ:
```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```


## Инференс модели
В файле <b>src/inference.py</b> в функцию predict передайте одну из 6 моделей из репозитория на [huggingface](https://huggingface.co/rhythm00)

![models](https://sun9-60.userapi.com/impf/JLVQEuE23UUlHHCcTPXHhBKUlSO2CnjQGXhNXw/kcgunsY3ALY.jpg?size=1920x644&quality=96&sign=92a51dbed6f717134fa4ce348feba884&type=album)

Выполните в терминале:
```bash
chmod +x solution

./solution your_type_sum path/to/your/dataset.jsonl path/to/your/result.jsonl
```

Где your_type_sum один из трех типов суммаризации:
  1. 'all_comments'
  2. 'post_comments'
  3. 'topic_comments'

path/to/your/dataset.jsonl - путь к оценочному датасету в формате:
![dataset_format](https://sun9-32.userapi.com/impf/xAc1edNOiowG6M8V25QxK8tz_R0YZvQd5JOZ7w/7-F5gGUhy_4.jpg?size=852x816&quality=96&sign=8c14087f352902f30afd23470985ef33&type=album)

path/to/your/result.jsonl - путь, куда сохранится результат суммаризации в формате .jsonl ![result_format](https://sun9-49.userapi.com/impf/A9yRael7RRlAnp-2j8srBKGto1eM_auM-CsJHA/7GzIv9qUoSY.jpg?size=1020x262&quality=96&sign=82115727bdb2956384ed5cb7368a6aad&type=album)

## Используемые наборы данных
- Датасет с постами и комментариями, который предоставили организаторы хакатона [>10000 постов с youtube, vk, telegram]. 
- Часть датасет с постами и комментариями с сайта DTF [>4000 постов] [статья про датасет](https://dtf.ru/u/169798-infernalnyj-gavnoed/2163613-vykladyvayu-dataset-s-kommentariyami-dtf-v-otkrytyj-dostup)
- IlyaGusev/gazeta [HF](https://huggingface.co/datasets/IlyaGusev/gazeta)


## Подход к решению задачи

<h4>0. EDA и первичная предобработка данных</h4>
<li> Провел базовую предобработку </li>
<li> почистил html разметку, URL </li>
<li> убрал слишком большие комментарии и комментарии без текста, которые появились после очистки html разметки</li>
<li>так как в дальнейшем планировалось использовать seq2seq модели на основе трансформер архитектуры, то пришлось так же отобрать только те посты, токенезированные комментарии к которым в сумме занимали меньше 1000 токенов (этот выбор продиктован квадратичным ростом потребляемой памяти у трансформер моделей). </li>

<h4>1. Разметка датасета</h4>
<p>Хороших датасетов для суммаризации на русском язык в открытом доступе мало, а для конкретной доменной области (комментариев под постами) их вообще нет.</p>

<p>Поэтому было решено разметить часть предобработанного датасета, который предоставили организаторы, с помощью OpenAI API chatgpt3.5. Так же дополнительно было размечено 4000 сэмплов датасета с постами и комментариями DTF.</p>


<h4>2. Эксперименты </h4>
Для экспериментов были выбраны модели на основе mT5 архитектуры:

- cointegrated/rut5-base-multitask
  Данная модель была дообучена на датасете организаторов + датасете DTF

  А так же отдельным экспериментом на датасете организаторов + датасете DTF + 50% датасета gazeta
- cointegrated/rut5-base
  Данная модель была дообучена на датасете организаторов + датасете DTF 
- cointegrated/rut5-small
  Для самой маленькой модели из семейства rut5 было проведено три эксперимента:
  1. только датасет организаторов + датасет DTF
  2. датасет организаторов + DTF + 50% gazeta
  3. датасет организаторов + DTF + 100% gazeta


![results](https://sun9-47.userapi.com/impf/RCscJQVbf87b3oLmIGB3rTBTgxr6GHU3gvBlyQ/V8EZbSaSdPc.jpg?size=1576x550&quality=96&sign=97b7876b057d37dad815e96e6bdc3eaf&type=album)

Разное количество итераций из-за разных датасетов и параметров batch_size и gradient_accumulation_steps.

Выводы: лучшей по качеству оказалась модель дообученная на датасете организаторов + DTF, т.е данные были из одной доменной области.

Лучшей из маленьких моделей rut5-small оказалась модель, обученная на смешанном датасете. 
